﻿// Data retrieved from http://vikjavev.no/ver/index.php?spenn=2d&sluttid=16.06.2015.
$(function () {
    Highcharts.chart('sap', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Average Rainfall'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: {
            categories: [
              '2014',
              '2015',
              '2016',
              '2017'
            ],
            crosshair: true
        },
        yAxis: {
            max: 150,
            title: {
                text: null
            }
        },
        title: {
            text: null,
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: null
        },
        credits: {
            enabled: false
        },
        colors: ['#38859B', '#46A1B9', '#7CBBCF', '#B5D5E1', '#CDE2EB', '#D9EAF0', '#4F6096', '#6C6550', 'green', 'lightblue', 'lightgreen'],
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.1f} Cơn</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Lốc',
            data: [100, 140,110, 115]

        }, {
            name: 'Mưa đá',
            data: [130, 120, 130, 125]

        }]
    });
});